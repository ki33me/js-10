/*
В папке tabs лежит разметка вкладок. При нажатии на вкладку отображается конкретный текст для нужной вкладки. При этом остальной 
текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.

Разметку можно изменять, добавлять нужные классы, ID, атрибуты, тэги.

Необходимо предусмотреть, что текст на вкладках может изменяться и вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.
*/

function tabs() {
    let nav = document.querySelectorAll(".tabs-title");
    let tabs = document.querySelectorAll(".tabs-content  li");

    for (let i = 0; i < nav.length; i++) {
        nav[i].dataset.nmb = `${i}`;
        tabs[i].dataset.nmb = `${i}`;
        nav[i].addEventListener("click", function () {
            for (let j = 0; j < nav.length; j++) {
                nav[j].classList.remove("active");
                tabs[j].classList.remove("active");
            }
            this.classList.add("active");
            document.querySelector(`.tabs-content li[data-nmb='${this.dataset.nmb}']`).classList.add("active");
        })
    }
}

tabs()
